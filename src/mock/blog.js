import { subDays, subHours } from "date-fns"; 

const now = new Date();

const blogs = [
    {
      id: 1,
      avatarUrl: '/assets/avatars/avatar-carson-darrin.png',
      title: "Kako da napisem dobar CV i motivaciono pismo",
      createdAt: subDays(subHours(now, 7), 1).getTime()
    },
    {
      id: 2,
      avatarUrl: '/assets/avatars/avatar-fran-perez.png',
      title: "Saveti kako da unapredite komunikacione vestine",
      createdAt: subDays(subHours(now, 7), 1).getTime()
    },
    {
      id: 3,
      avatarUrl: '/assets/avatars/avatar-jie-yan-song.png',
      title: "Zasto je vazno ici na praksu tokom studiranja?",
      createdAt: subDays(subHours(now, 1), 2).getTime()
    }
  ];
  export default blogs;