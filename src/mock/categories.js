import { subDays, subHours } from "date-fns";

const now = new Date();

const categories = [
  {
    id: 1,
    avatarUrl: '/assets/avatars/avatar-carson-darrin.png',
    category: "Administracija",
    createdAt: subDays(subHours(now, 7), 1).getTime()
  },
  {
    id: 2,
    avatarUrl: '/assets/avatars/avatar-fran-perez.png',
    category: "Ugostiteljstvo",
    createdAt: subDays(subHours(now, 7), 1).getTime()
  },
  {
    id: 3,
    avatarUrl: '/assets/avatars/avatar-jie-yan-song.png',
    category: "Deklaracije",
    createdAt: subDays(subHours(now, 1), 2).getTime()
  },
  {
    id: 4,
    avatarUrl: '/assets/avatars/avatar-anika-visser.png',
    category: "Laki fizicki poslovi",
  },
  {
    id: 5,
    avatarUrl: '/assets/avatars/avatar-anika-visser.png',
    category: "Teski fizicki poslovi",
    createdAt: subDays(subHours(now, 4), 2).getTime()
  },
];
export default categories;
