
import { sample } from "lodash";

const users = [
    {
      id: '5e887ac47eed253091be10cb',
      email: "user1@gmail.com",
      date: 1554670800000
    },
    {
      id: '5e887b209c28ac3dd97f6db5',    
      email: 'user2@gmail.com',
      date: 1554670800000,
    },
    {
      id: '5e887b7602bdbc4dbb234b27',
      email: 'user3@gmail.com',
      date: 1554670800000,
     
    },
    {
      id: '5e86809283e28b96d2d38537',
      email: 'user4@gmail.com',
      date: 1554757200000
    },
    {
      id: '5e86805e2bafd54f66cc95c3',
      email: 'user5@gmail.com',
      date: 1554930000000
    },
    {
      id: '5e887a1fbefd7938eea9c981',
      email: 'user6@gmail.com',
      date: 1554930000000
    },
    {
      id: '5e887d0b3d090c1b8f162003',
      email: 'user7@gmail.com',
      date: 1554670800000,
    },
  ];

export default users;
