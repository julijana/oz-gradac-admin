import { createContext, useContext, useEffect, useReducer, useRef, useState } from 'react';

import PropTypes from 'prop-types';

// The role of this context is to propagate authentication state through the App tree.

export const AuthContext = createContext({ undefined });

export const AuthProvider = (props) => {
  const { children } = props;
  const [token, setToken] = useState(() => {

    let accessToken = typeof window !== 'undefined' ? localStorage.getItem("accessToken") : null;
    if (accessToken) {
        return accessToken;
    }
    return null;
});

  const [isAuthenticated, setIsAuthenticated]= useState(() => {
    console.log(token);
    let authenticated = token !== null ? true : false;
    return authenticated;
  });
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const loginUser = async (user) => {
    setIsLoading(true);
    const res = await fetch('http://localhost:8080/api/auth/signin',
      {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(user)
      })
    if (!res.ok) {
      console.log("Error");
    } else {
      const data = await res.json();
      console.log(data.jwt);
      localStorage.setItem("accessToken", data.jwt);
      setToken(data.jwt);
      setUser(data.user);
      setIsAuthenticated(true);
      console.log(user);
    }
    setIsLoading(false);
  };

  const registerUser = async (user) => {
    setIsLoading(true);
    const res = await fetch('http://localhost:8080/api/auth/register',
      {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify(user)
      })
    if (!res.ok) {
      console.log("Error");
    } else {
      const data = await res.json();
      console.log(data);
    }
    setIsLoading(false);
  }

  const logout = () => {
    if (typeof window !== 'undefined' && window.localStorage) {
      localStorage.removeItem('accessToken');
      setUser(null);
      setToken(null);
      setIsAuthenticated(false);
    }
  }

  return (
    <AuthContext.Provider
      value={{
        user,
        token,
        isAuthenticated,
        isLoading,
        loginUser,
        registerUser,
        logout
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.node
};

export const AuthConsumer = AuthContext.Consumer;

export const useAuthContext = () => useContext(AuthContext);