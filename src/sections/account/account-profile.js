import {
    Avatar,
    Box,
    Button,
    Card,
    CardActions,
    CardContent,
    Divider,
    Typography,
  } from "@mui/material";
  
  const user = {
    avatar: "/assets/logo.png",
    city: "admin@ozgradac.rs",
    country: "USA",
    jobTitle: "Senior Developer",
    name: "Admin Admin",
    timezone: "Beograd",
  };
  
  export const AccountProfile = () => (
    <Card>
      <CardContent>
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Avatar
            src={user.avatar}
            sx={{
              height: 80,
              mb: 2,
              width: 80,
            }}
          />
          <Typography gutterBottom variant="h5">
            {user.name}
          </Typography>
          <Typography color="text.secondary" variant="body2">
            {user.city}
          </Typography>
          <Typography color="text.secondary" variant="body2">
            {user.timezone}
          </Typography>
        </Box>
      </CardContent>
      <Divider />
      <CardActions>
        <Button fullWidth variant="text">
          Promeni sliku
        </Button>
      </CardActions>
    </Card>
  );
  