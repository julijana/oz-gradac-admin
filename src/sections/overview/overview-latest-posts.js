import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardHeader,
  Divider,
  Stack,
  SvgIcon,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";

import ArrowRightIcon from "@heroicons/react/24/solid/ArrowRightIcon";
import Link from "next/link";
import PropTypes from "prop-types";
import { Scrollbar } from "src/components/scrollbar";
import { SeverityPill } from "src/components/severity-pill";
import { getInitials } from "src/utils/get-initials";
import moment from "moment/moment";

const statusMap = {
  "Ucenici i studenti": "warning",
  "Studenti": "success",
  "Nezaposleni do 30 god": "error",
  "Svi": "info",
};

const gendersMap = {
  "Momci": "info",
  "Devojke": "error",
  "Momci i devojke": "warning",
};

export const OverviewLatestPosts = (props) => {
  const { orders = [], sx } = props;

  return (
    <Card sx={sx}>
      <CardHeader title="Najnoviji postovi" />
      <Scrollbar sx={{ flexGrow: 1 }}>
        <Box sx={{ minWidth: 1500 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell sortDirection="desc">Datum kreiranja</TableCell>
                <TableCell>Kategorija</TableCell>
                <TableCell>Naslov</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Cena</TableCell>
                <TableCell>Lokacija</TableCell>
                <TableCell>Pol</TableCell>
                <TableCell>Radno vreme</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {orders.map((order) => {
                return (
                  <TableRow hover key={order.id}>
                    <TableCell>{moment(order.created).format('DD.MM.YYYY.')}</TableCell>
                    <Stack alignItems="center" direction="row" spacing={2}>
                      <Avatar src={"/assets/avatars/avatar-carson-darrin.png"}>
                        {order.category}
                      </Avatar>
                      <Typography>{order.category.category}</Typography>
                    </Stack>
                    <TableCell>{order.title}</TableCell>
                    <TableCell>
                      <SeverityPill color={statusMap[order.status]}>
                        {order.status}
                      </SeverityPill>
                    </TableCell>
                    <TableCell>{order.price}</TableCell>
                    <TableCell>{order.location}</TableCell>
                    <TableCell>
                      <SeverityPill color={gendersMap[order.gender]}>
                        {order.gender}
                      </SeverityPill>
                    </TableCell>
                    <TableCell>{order.workTime}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </Scrollbar>
      <Divider />
      <CardActions sx={{ justifyContent: "flex-end" }}>
        <Link href="/posts">
          <Button
            color="inherit"
            endIcon={
              <SvgIcon fontSize="small">
                <ArrowRightIcon />
              </SvgIcon>
            }
            size="small"
            variant="text"
          >
            Pogledaj sve
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
};

OverviewLatestPosts.prototype = {
  orders: PropTypes.array,
  sx: PropTypes.object,
};
