import {
  Box,
  Button,
  Card,
  CardActions,
  CardHeader,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  SvgIcon,
} from "@mui/material";

import ArrowRightIcon from "@heroicons/react/24/solid/ArrowRightIcon";
import EllipsisVerticalIcon from "@heroicons/react/24/solid/EllipsisVerticalIcon";
import Link from "next/link";
import PropTypes from "prop-types";
import { formatDistanceToNow } from "date-fns";
import moment from "moment/moment";

export const OverviewLatestCategories = (props) => {
  const { categories = [], sx } = props;

  return (
    <Card sx={sx}>
      <CardHeader title="Najaktuelnije kategorije" />
      <List>
        {categories.map((category, index) => {
          const hasDivider = index < category.length - 1;

          return (
            <ListItem divider={hasDivider} key={category.id}>
              <ListItemAvatar>
                <Box
                  component="img"
                  src={"/assets/avatars/avatar-carson-darrin.png"}
                  sx={{
                    borderRadius: 1,
                    height: 48,
                    width: 48,
                  }}
                />
                {/* {category.avatarUrl ? (
                  <Box
                    component="img"
                    src={category.avatarUrl}
                    sx={{
                      borderRadius: 1,
                      height: 48,
                      width: 48,
                    }}
                  />
                ) : (
                  <Box
                    sx={{
                      borderRadius: 1,
                      backgroundColor: "neutral.200",
                      height: 48,
                      width: 48,
                    }}
                  />
                )} */}
              </ListItemAvatar>
              <ListItemText
                primary={category.category}
                primaryTypographyProps={{ variant: "subtitle1" }}
                secondary={`Kreirano ${moment(category.created).format('DD.MM.YYYY.')}`}
                secondaryTypographyProps={{ variant: "body2" }}
              />
            </ListItem>
          );
        })}
      </List>
      <Divider />
      <CardActions sx={{ justifyContent: "flex-end" }}>
        <Link href="/kategorije">
          <Button
            color="inherit"
            endIcon={
              <SvgIcon fontSize="small">
                <ArrowRightIcon />
              </SvgIcon>
            }
            size="small"
            variant="text"
          >
            Pogledaj sve
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
};

OverviewLatestCategories.propTypes = {
  products: PropTypes.array,
  sx: PropTypes.object,
};
