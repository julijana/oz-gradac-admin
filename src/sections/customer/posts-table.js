import {
  Avatar,
  Box,
  Button,
  Card,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  IconButton,
  MenuItem,
  Popover,
  Stack,
  SvgIcon,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";

import EllipsisVerticalIcon from "@heroicons/react/24/solid/EllipsisVerticalIcon";
import Iconify from "@/src/components/iconify/Iconify";
import Link from "next/link";
import PropTypes from "prop-types";
import { Scrollbar } from "src/components/scrollbar";
import { SeverityPill } from "src/components/severity-pill";
import { format } from "date-fns";
import { getInitials } from "src/utils/get-initials";
import { useState } from "react";

const statusMap = {
  "Ucenici i studenti": "warning",
  Studenti: "success",
  "Nezaposleni do 30 god": "error",
  Svi: "info",
};

const gendersMap = {
  Momci: "info",
  Devojke: "error",
  "Momci i devojke": "warning",
};

export const PostsTable = (props) => {
  const {
    items = [],
    count = 0,
    onPageChange = () => {},
    onRowsPerPageChange,
    page = 0,
    rowsPerPage = 0,
    sx,
  } = props;
  const [open, setOpen] = useState(null);
  const [post, setPost] = useState(null);
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <>
      <Card sx={sx}>
        <Scrollbar sx={{ flexGrow: 1 }}>
          <Box sx={{ minWidth: 1600 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell sortDirection="desc">Datum kreiranja</TableCell>
                  <TableCell>Kategorija</TableCell>
                  <TableCell>Naslov</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell>Cena</TableCell>
                  <TableCell>Lokacija</TableCell>
                  <TableCell>Pol</TableCell>
                  <TableCell>Radno vreme</TableCell>
                  <TableCell>Akcije</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {items.map((item) => {
                  return (
                    <TableRow hover key={item.postId}>
                      <TableCell>{moment(item.created).format('DD.MM.YYYY.')}</TableCell>
                      <TableCell>
                        <Stack alignItems="center" direction="row" spacing={2}>
                          <Avatar src={"/assets/avatars/avatar-anika-visser.png"}>
                            {getInitials(item.category)}
                          </Avatar>
                          <Typography>{item.category}</Typography>
                        </Stack>
                      </TableCell>
                      <TableCell>{item.title}</TableCell>
                      <TableCell>
                        <SeverityPill color={statusMap[item.status]}>
                          {item.status}
                        </SeverityPill>
                      </TableCell>
                      <TableCell>{item.price}</TableCell>
                      <TableCell>{item.location}</TableCell>
                      <TableCell>
                        <SeverityPill color={gendersMap[item.gender]}>
                          Momci i devojke
                        </SeverityPill>
                      </TableCell>
                      <TableCell>{item.workTime}</TableCell>
                      <TableCell>
                        <IconButton
                          edge="end"
                          onClick={(e) => {
                            setOpen(e.currentTarget), setPost(item);
                          }}
                        >
                          <SvgIcon>
                            <EllipsisVerticalIcon />
                          </SvgIcon>
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Box>
        </Scrollbar>
        <TablePagination
          component="div"
          count={count}
          onPageChange={onPageChange}
          onRowsPerPageChange={onRowsPerPageChange}
          labelRowsPerPage="Redova po strani:"
          page={page}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[5, 10, 25]}
        />
      </Card>
      <Popover
        open={Boolean(open)}
        anchorEl={open}
        onClose={() => setOpen(null)}
        anchorOrigin={{ vertical: "top", horizontal: "left" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        PaperProps={{
          sx: {
            p: 1,
            width: 140,
            "& .MuiMenuItem-root": {
              px: 1,
              typography: "body2",
              borderRadius: 0.75,
            },
          },
        }}
      >
        <MenuItem
          component={Link}
          href={{ pathname: "/izmeni-objavu", query: post }}
        >
          <Iconify icon={"eva:edit-fill"} sx={{ mr: 2 }} />
          Izmeni
        </MenuItem>

        <MenuItem
          sx={{ color: "error.main" }}
          onClick={() => {
            setDialogOpen(true);
          }}
        >
          <Iconify icon={"eva:trash-2-outline"} sx={{ mr: 2 }} />
          Obrisi
        </MenuItem>
      </Popover>
      <Dialog
        open={dialogOpen}
        onClose={() => setDialogOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Da li ste sigurni da zelite da obrisete ovaj post?"}
        </DialogTitle>
        <Divider />
        <DialogContent>
          {post && (
            <DialogContentText id="alert-dialog-description">
              Kategorija: {post.category} <br />
              Datum kreiranja: {format(post.createdAt, "dd/MM/yyyy")} <br />
              Naslov: {post.title} <br />
              Status: {post.status} <br />
              Lokacija: {post.location} <br />
              Pol: {post.gender} <br />
              Radno vreme: {post.workTime} <br />
              Cena: {post.price}
            </DialogContentText>
          )}
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button onClick={() => setDialogOpen(false)}>Ne</Button>
          <Button onClick={() => setDialogOpen(false)} autoFocus>
            Da
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

PostsTable.prototype = {
  orders: PropTypes.array,
  sx: PropTypes.object,
};
