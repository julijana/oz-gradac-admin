import {
  Avatar,
  Box,
  Button,
  Card,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  IconButton,
  MenuItem,
  Popover,
  Stack,
  SvgIcon,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";

import EllipsisVerticalIcon from "@heroicons/react/24/solid/EllipsisVerticalIcon";
import Iconify from "@/src/components/iconify/Iconify";
import Link from "next/link";
import PropTypes from "prop-types";
import { Scrollbar } from "src/components/scrollbar";
import { getInitials } from "src/utils/get-initials";
import moment from "moment/moment";
import { useState } from "react";

export const CategoriesTable = (props) => {
  const [open, setOpen] = useState(null);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [category, setCategory] = useState(null);

  const {
    count = 0,
    items = [],
    onPageChange = () => {},
    onRowsPerPageChange,
    page = 0,
    rowsPerPage = 0,
  } = props;

  const handleCloseMenu = () => {
    setOpen(null);
  };

  return (
    <>
      <Card>
        <Scrollbar>
          <Box sx={{ minWidth: 800 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Kategorija</TableCell>
                  <TableCell>Datum kreiranja</TableCell>
                  <TableCell>Akcije</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {items.map((category) => {
                  return (
                    <TableRow hover key={category.id}>
                      <TableCell>
                        <Stack alignItems="center" direction="row" spacing={2}>
                          <Avatar src={'/assets/avatars/avatar-carson-darrin.png'}>
                            {category.category}
                          </Avatar>
                          <Typography>{category.category}</Typography>
                        </Stack>
                      </TableCell>
                      <TableCell>{moment(category.created).format('DD.MM.YYYY.')}</TableCell>
                      <TableCell>
                        <IconButton
                          edge="end"
                          onClick={(event) => {
                            setCategory(category), setOpen(event.currentTarget);
                          }}
                        >
                          <SvgIcon>
                            <EllipsisVerticalIcon />
                          </SvgIcon>
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Box>
        </Scrollbar>
        <TablePagination
          component="div"
          count={count}
          onPageChange={onPageChange}
          onRowsPerPageChange={onRowsPerPageChange}
          labelRowsPerPage="Redova po strani:"
          page={page}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[5, 10, 25]}
        />
      </Card>
      <Popover
        open={Boolean(open)}
        anchorEl={open}
        onClose={() => setOpen(null)}
        anchorOrigin={{ vertical: "top", horizontal: "left" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        PaperProps={{
          sx: {
            p: 1,
            width: 140,
            "& .MuiMenuItem-root": {
              px: 1,
              typography: "body2",
              borderRadius: 0.75,
            },
          },
        }}
      >
        <MenuItem
          component={Link}
          href={{ pathname: "/izmeni-kategoriju", query: category }}
        >
          <Iconify icon={"eva:edit-fill"} sx={{ mr: 2 }} />
          Izmeni
        </MenuItem>

        <MenuItem
          sx={{ color: "error.main" }}
          onClick={() => {
            setDialogOpen(true);
          }}
        >
          <Iconify icon={"eva:trash-2-outline"} sx={{ mr: 2 }} />
          Obrisi
        </MenuItem>
      </Popover>
      <Dialog
        open={dialogOpen}
        onClose={() => setDialogOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Da li ste sigurni da zelite da obrisete ovu kategoriju?"}
        </DialogTitle>
        <Divider/>
        <DialogContent>
          {category && (
            <DialogContentText id="alert-dialog-description">
              Kategorija: {category.category} <br/>
              Datum kreiranja: 22/08/2023
            </DialogContentText>
          )}
        </DialogContent>
        <Divider/>
        <DialogActions>
          <Button onClick={() => setDialogOpen(false)}>Ne</Button>
          <Button onClick={() => setDialogOpen(false)} autoFocus>
            Da
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

CategoriesTable.propTypes = {
  count: PropTypes.number,
  items: PropTypes.array,
  onDeselectAll: PropTypes.func,
  onDeselectOne: PropTypes.func,
  onPageChange: PropTypes.func,
  onRowsPerPageChange: PropTypes.func,
  onSelectAll: PropTypes.func,
  onSelectOne: PropTypes.func,
  page: PropTypes.number,
  rowsPerPage: PropTypes.number,
  selected: PropTypes.array,
};
