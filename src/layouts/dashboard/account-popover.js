import { Box, Divider, MenuItem, MenuList, Popover, Typography } from '@mui/material';
import { useCallback, useEffect, useState } from 'react';

import PropTypes from 'prop-types';
import { useAuth } from 'src/hooks/use-auth';
import { useRouter } from 'next/navigation';

export const AccountPopover = (props) => {
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const { anchorEl, onClose, open } = props;
  const router = useRouter();
  const auth = useAuth();
  const token = auth.token;

  useEffect(() => {
    getUser();
  }, [auth, router])

  const getUser = async () => {
    setIsLoading(true);
    const res = await fetch('http://localhost:8080/api/user/',
      {
        method: 'GET',
        headers: {
          'Content-type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      });

    if (!res.ok) {
      console.log("Error");
    } else {
      const data = await res.json();
      setUser(data);
    }
    setIsLoading(false);
  };

  const handleSignOut = useCallback(
    () => {
      onClose?.();
      auth.logout();
      router.push('/auth/login');
    },
    [onClose, auth, router]
  );

  return (
    <Popover
      anchorEl={anchorEl}
      anchorOrigin={{
        horizontal: 'left',
        vertical: 'bottom'
      }}
      onClose={onClose}
      open={open}
      PaperProps={{ sx: { width: 200 } }}
    >
      <Box
        sx={{
          py: 1.5,
          px: 2
        }}
      >
        <Typography variant="overline">
          Nalog
        </Typography>
        <Typography
          color="text.secondary"
          variant="body2"
        >
          {user ? user.firstName + " " + user.lastName : ""}
        </Typography>
      </Box>
      <Divider />
      <MenuList
        disablePadding
        dense
        sx={{
          p: '8px',
          '& > *': {
            borderRadius: 1
          }
        }}
      >
        <MenuItem onClick={handleSignOut}>
          Odjavi se
        </MenuItem>
      </MenuList>
    </Popover>
  );
};

AccountPopover.propTypes = {
  anchorEl: PropTypes.any,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};