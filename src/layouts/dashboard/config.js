import ChartBarIcon from '@heroicons/react/24/solid/ChartBarIcon';
import CogIcon from '@heroicons/react/24/solid/CogIcon';
import LockClosedIcon from '@heroicons/react/24/solid/LockClosedIcon';
import ShoppingBagIcon from '@heroicons/react/24/solid/ShoppingBagIcon';
import NewspaperIcon from '@heroicons/react/24/solid/NewspaperIcon';
import QuestionMarkCircle from '@heroicons/react/24/solid/QuestionMarkCircleIcon';
import DocumentTextIcon from '@heroicons/react/24/solid/DocumentTextIcon';
import UserPlusIcon from '@heroicons/react/24/solid/UserPlusIcon';
import UsersIcon from '@heroicons/react/24/solid/UsersIcon';
import ListBullet from '@heroicons/react/24/solid/ListBulletIcon';
import XCircleIcon from '@heroicons/react/24/solid/XCircleIcon';
import { SvgIcon } from '@mui/material';

export const items = [
  {
    title: 'Pregled',
    path: '/',
    icon: (
      <SvgIcon fontSize="small">
        <ChartBarIcon />
      </SvgIcon>
    )
  },
  {
    title: 'Kategorije',
    path: '/kategorije',
    icon: (
      <SvgIcon fontSize="small">
        <ListBullet />
      </SvgIcon>
    )
  },
  {
    title: 'Objave',
    path: '/posts',
    icon: (
      <SvgIcon fontSize="small">
        <DocumentTextIcon />
      </SvgIcon>
    )
  },
  {
    title: 'Sabskrajbovani korisnici',
    path: '/sabskrajbovani-korisnici',
    icon: (
      <SvgIcon fontSize="small">
        <UsersIcon />
      </SvgIcon>
    )
  },
  {
    title: 'FAQ\'s',
    path: '/cesta-pitanja',
    icon: (
      <SvgIcon fontSize="small">
        <QuestionMarkCircle />
      </SvgIcon>
    )
  },
 
  {
    title: 'Korisni tekstovi',
    path: '/korisni-tekstovi',
    icon: (
      <SvgIcon fontSize="small">
        <NewspaperIcon />
      </SvgIcon>
    )
  }
];