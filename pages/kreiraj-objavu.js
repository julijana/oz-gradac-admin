import Head from "next/head";
import { useFormik } from "formik";
import useMediaQuery from "@mui/material/useMediaQuery";
import * as yup from "yup";
import { Box, Button, TextField, Typography, MenuItem } from "@mui/material";
import { useAuth } from "src/hooks/use-auth";
import { Layout as DashboardLayout } from "src/layouts/dashboard/layout";
import categories from "@/src/mock/categories";
import statuses from "@/src/mock/statuses";
import genders from "@/src/mock/genders";

const Page = () => {
  const auth = useAuth();
  const isNonMobile = useMediaQuery("(min-width:600px)");

  const checkoutSchema = yup.object().shape({
    title: yup.string().required("Ovo polje je obavezno"),
    category: yup.string().required("Ovo polje je obavezno"),
    description: yup.string().required("Ovo polje je obavezno"),
    status: yup.string().required("Ovo polje je obavezno"),
    workTime: yup.string().required("Ovo polje je obavezno"),
    price: yup.string().required("Ovo polje je obavezno"),
    location: yup.string().required("Ovo polje je obavezno"),
    gender: yup.string().required("Ovo polje je obavezno"),
  });

  const onSubmit = async (values, actions) => {
    console.log(values);
    // const data = {
    //   category: values.category,
    // };
    // createCategory(data);
    await new Promise((resolve) => setTimeout(resolve, 1000));
    actions.resetForm();
  };

  const {
    values,
    handleBlur,
    isSubmitting,
    handleChange,
    handleSubmit,
    touched,
    errors,
  } = useFormik({
    initialValues: {
      title: "",
      category: "",
      status: "",
      workTime: "",
      price: "",
      location: "",
      gender: "",
      tags: "",
      description: "",
    },
    validationSchema: checkoutSchema,
    onSubmit,
  });

  return (
    <>
      <Head>
        <title>Kreiraj objavu | Devias Kit</title>
      </Head>
      <Box m="20px">
        <Box mb="30px">
          <Typography variant="h4">Kreiraj objavu</Typography>
        </Box>
        <form onSubmit={handleSubmit}>
          <Box
            display="grid"
            gap="30px"
            gridTemplateColumns="repeat(3, minmax(0, 1fr))"
            sx={{
              "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
            }}
          >
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Naslov"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.title}
              name="title"
              error={!!touched.title && !!errors.title}
              helperText={touched.title && errors.title}
              sx={{ gridColumn: "span 1" }}
            />
            <TextField
              fullWidth
              variant="filled"
              select
              label="Kategorija"
              onChange={handleChange}
              value={values.category}
              name="category"
              error={!!touched.category && !!errors.category}
              helperText={touched.category && errors.category}
              sx={{ gridColumn: "span 1" }}
            >
              {categories.map((item, pos) => {
                return (
                  <MenuItem key={pos} value={item.category}>
                    {item.category}
                  </MenuItem>
                );
              })}
            </TextField>
            <TextField
              fullWidth
              variant="filled"
              select
              label="Status"
              onChange={handleChange}
              value={values.status}
              name="status"
              error={!!touched.status && !!errors.status}
              helperText={touched.status && errors.status}
              sx={{ gridColumn: "span 1" }}
            >
              {statuses.map((item, pos) => {
                return (
                  <MenuItem key={pos} value={item.status}>
                    {item.status}
                  </MenuItem>
                );
              })}
            </TextField>
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Radno vreme"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.workTime}
              name="workTime"
              error={!!touched.workTime && !!errors.workTime}
              helperText={touched.workTime && errors.workTime}
              sx={{ gridColumn: "span 1" }}
            />
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Cena"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.price}
              name="price"
              error={!!touched.price && !!errors.price}
              helperText={touched.price && errors.price}
              sx={{ gridColumn: "span 1" }}
            />
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Lokacija"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.location}
              name="location"
              error={!!touched.location && !!errors.location}
              helperText={touched.location && errors.location}
              sx={{ gridColumn: "span 1" }}
            />
            <TextField
              fullWidth
              variant="filled"
              select
              label="Pol"
              onChange={handleChange}
              value={values.gender}
              name="gender"
              error={!!touched.gender && !!errors.gender}
              helperText={touched.gender && errors.gender}
              sx={{ gridColumn: "span 1" }}
            >
              {genders.map((item, pos) => {
                return (
                  <MenuItem key={pos} value={item.gender}>
                    {item.gender}
                  </MenuItem>
                );
              })}
            </TextField>
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Tagovi"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.tags}
              name="tags"
              error={!!touched.tags && !!errors.tags}
              helperText={touched.tags && errors.tags}
              sx={{ gridColumn: "span 2" }}
            />
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Opis"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.description}
              name="description"
              multiline={true}
              rows={10}
              error={!!touched.description && !!errors.description}
              helperText={touched.description && errors.description}
              sx={{ gridColumn: "span 3" }}
            />
          </Box>
          <Box display="flex" justifyContent="end" mt="20px">
            <Button variant="contained" disabled={isSubmitting} type="submit">
              Sacuvaj
            </Button>
          </Box>
        </form>
      </Box>
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
