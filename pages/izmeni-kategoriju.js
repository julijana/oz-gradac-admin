import * as yup from "yup";

import { Box, Button, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";

import { Layout as DashboardLayout } from "src/layouts/dashboard/layout";
import Head from "next/head";
import Thumb from "@/src/sections/category/upload-images";
import { useAuth } from "src/hooks/use-auth";
import { useFormik } from "formik";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useRouter } from "next/router";

const Page = () => {

  const router = useRouter();
  const auth = useAuth();
  const isNonMobile = useMediaQuery("(min-width:600px)");


  const checkoutSchema = yup.object().shape({
    category: yup.string().required("Ovo polje je obavezno"),
    file: yup.mixed().required(),
  });

  const onSubmit = async (values, actions) => {
    console.log(values);
    const data = {
      category: values.category,
    };
    editCategory(data);
    await new Promise((resolve) => setTimeout(resolve, 1000));
    actions.resetForm();
  };

  const editCategory = async (data) => {
    const res = await fetch("http://localhost:8080/api/posts/category", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(data),
    });
    if (!res.ok) {
      console.log("Error");
    } else {
      const data = await res.json();
      console.log(data);
      router.push("/kategorije");
    }
  };

  const {
    values,
    setFieldValue,
    handleBlur,
    isSubmitting,
    handleChange,
    handleSubmit,
    touched,
    errors,
  } = useFormik({
    initialValues: {
      category: router.query.category,
      file: null,
    },
    validationSchema: checkoutSchema,
    onSubmit,
  });

  return (
    <>
      <Head>
        <title>Izmeni kategoriju | Omladinska zadruga Gradac</title>
      </Head>
      <Box m="20px">
        <Box mb="30px">
          <Typography variant="h4">Izmeni kategoriju</Typography>
        </Box>
        <form onSubmit={handleSubmit}>
          <Box
            display="grid"
            gap="30px"
            gridTemplateColumns="repeat(3, minmax(0, 1fr))"
            sx={{
              "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
            }}
          >
             <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Kategorija"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.category}
              name="category"
              error={!!touched.category && !!errors.category}
              helperText={touched.category && errors.category}
              sx={{ gridColumn: "span 1" }}
            />
            <TextField
              id="file"
              name="file"
              type="file"
              onChange={(event) => {
                setFieldValue("file", event.currentTarget.files[0]);
              }}
              sx={{ gridColumn: "span 2" }}
            />
            <Box>
              <Thumb file={values.file} />
            </Box>
          </Box>
          <Box display="flex" justifyContent="start" mt="20px">
            <Button variant="contained" disabled={isSubmitting} type="submit">
              Sacuvaj
            </Button>
          </Box>
        </form>
      </Box>
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
