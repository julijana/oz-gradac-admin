import * as Yup from "yup";

import { Box, Button, Stack, TextField, Typography } from "@mui/material";
import { useCallback, useEffect, useState } from "react";

import { Layout as AuthLayout } from "src/layouts/auth/layout";
import Head from "next/head";
import Image from "next/image";
import NextLink from "next/link";
import logo from "../../public/assets/vektorski-logo.png";
import { useAuth } from "src/hooks/use-auth";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";

const Page = () => {
  const router = useRouter();
  const auth = useAuth();
  const isAuthenticated = auth.isAuthenticated;
  const [method, setMethod] = useState("email");
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
      submit: null,
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .max(255)
        .required("Email je obavezan"),
      password: Yup.string().max(255).required("Sifra je obavezna"),
    }),
    onSubmit: async (values, helpers) => {
      try {
        const data = {
          username: values.email,
          password: values.password
        }
        await auth.loginUser(data);
        router.push("/");
      } catch (err) {
        helpers.setStatus({ success: false });
        helpers.setErrors({ submit: err.message });
        helpers.setSubmitting(false);
      }
    },
  });

  useEffect(() => {
    checkAuthorization();
  }, [auth, router])

  const checkAuthorization = async () => {
    if (isAuthenticated) {
      router.push('/');
      return null;
    }
  };

  const handleSkip = useCallback(() => {
    auth.skip();
    router.push("/");
  }, [auth, router]);

  return (
    <>
      <Head>
        <title>Login | Omladinska zadruga Gradac</title>
      </Head>
      <Box
        sx={{
          backgroundColor: "background.paper",
          flex: "1 1 auto",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Box
          sx={{
            maxWidth: 550,
            px: 3,
            py: "100px",
            width: "100%",
          }}
        >
          <div>
            <Stack spacing={1} sx={{ mb: 3 }}>
              <Box
                component={NextLink}
                href="/"
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  marginBottom: "50px",
                }}
              >
                <Image
                  alt="profile-user"
                  width={160}
                  height={80}
                  src={logo}
                  style={{ cursor: "pointer" }}
                />
              </Box>
              <Typography variant="h4">Uloguj se</Typography>
            </Stack>
            <form noValidate onSubmit={formik.handleSubmit}>
              <Stack spacing={3}>
                <TextField
                  error={!!(formik.touched.email && formik.errors.email)}
                  fullWidth
                  helperText={formik.touched.email && formik.errors.email}
                  label="Korisnicko ime"
                  name="email"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="email"
                  value={formik.values.email}
                />
                <TextField
                  error={!!(formik.touched.password && formik.errors.password)}
                  fullWidth
                  helperText={formik.touched.password && formik.errors.password}
                  label="Sifra"
                  name="password"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="password"
                  value={formik.values.password}
                />
              </Stack>
              {formik.errors.submit && (
                <Typography color="error" sx={{ mt: 3 }} variant="body2">
                  {formik.errors.submit}
                </Typography>
              )}
              <Button
                fullWidth
                size="large"
                sx={{ mt: 3 }}
                type="submit"
                variant="contained"
              >
                Nastavi
              </Button>
              <Button
                fullWidth
                size="large"
                sx={{ mt: 3 }}
                onClick={handleSkip}
              >
                Preskoci autentikaciju
              </Button>
            </form>
          </div>
        </Box>
      </Box>
    </>
  );
};

Page.getLayout = (page) => <AuthLayout>{page}</AuthLayout>;

export default Page;
