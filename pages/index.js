import { Box, Container, Unstable_Grid2 as Grid } from "@mui/material";
import { subDays, subHours } from "date-fns";
import { useEffect, useRef, useState } from "react";

import { Layout as DashboardLayout } from "src/layouts/dashboard/layout";
import Head from "next/head";
import { OverviewBudget } from "src/sections/overview/overview-budget";
import { OverviewLatestCategories } from "src/sections/overview/overview-latest-categories";
import { OverviewLatestPosts } from "src/sections/overview/overview-latest-posts";
import { OverviewTotalCustomers } from "src/sections/overview/overview-total-customers";
import { OverviewTotalQuestions } from "@/src/sections/overview/overview-total-questions";
import { OverviewTotalTexts } from "@/src/sections/overview/overview-total-texts";
import { useAuth } from 'src/hooks/use-auth';

const Page = () => {

  const [posts, setPosts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(false);
  const auth = useAuth();
  const token = auth.token;

  useEffect(() => {
    const loadPosts = async () => {
      const postsFromServer = await fetchPosts();
      setPosts(postsFromServer);
    }
    loadPosts();
    loadCategories();
  }, [])

  const loadCategories = async () => {
    const categoriesFromServer = await fetchCategories();
    setCategories(categoriesFromServer);
  }

  const fetchCategories = async () => {
    setLoading(true);
    const res = await fetch('http://localhost:8080/api/category/',
      {
        method: 'GET',
        headers: {
          'Content-type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      });

    if (!res.ok) {
      console.log("Error");
    } else {
      const data = await res.json();
      return data;
    }
    setLoading(false);
  }

  const fetchPosts = async () => {
    setLoading(true);
    const res = await fetch('http://localhost:8080/api/posts/',
      {
        method: 'GET',
        headers: {
          'Content-type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      });

    if (!res.ok) {
      console.log("Error");
      setLoading(false);
    } else {
      const data = await res.json();
      setLoading(false);
      return data;
    }
  }
  return (
    <>
      <Head>
        <title>Pregled | Omladinska zadruga Gradac</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="xl">
          {!loading && (
            <Grid container spacing={3}>
              <Grid xs={12} sm={6} lg={3}>
                <OverviewBudget sx={{ height: "100%" }} value={posts.length} />
              </Grid>
              <Grid xs={12} sm={6} lg={3}>
                <OverviewTotalCustomers sx={{ height: "100%" }} value="1.6k" />
              </Grid>
              <Grid xs={12} sm={6} lg={3}>
                <OverviewTotalQuestions sx={{ height: "100%" }} value="500" />
              </Grid>
              <Grid xs={12} sm={6} lg={3}>
                <OverviewTotalTexts sx={{ height: "100%" }} value="10" />
              </Grid>

              <Grid xs={12} md={6} lg={4}>
                <OverviewLatestCategories
                  categories={categories}
                  sx={{ height: "100%" }}
                />
              </Grid>
              <Grid xs={12} md={12} lg={8}>
                <OverviewLatestPosts
                  orders={posts}
                  sx={{ height: "100%" }}
                />
              </Grid>
            </Grid>
          )}
        </Container>
      </Box>
    </>
  )
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
