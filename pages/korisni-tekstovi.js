import {
  Box,
  Button,
  Container,
  Grid,
  Paper,
  Stack,
  SvgIcon,
  Typography,
} from "@mui/material";

import { Layout as DashboardLayout } from "src/layouts/dashboard/layout";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import PlusIcon from "@heroicons/react/24/solid/PlusIcon";
import blogs from "@/src/mock/blog";
import { experimentalStyled as styled } from "@mui/material/styles";

const Page = () => {

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: "fff",
    ...theme.typography.body2,
    padding: theme.spacing(2),
    boxShadow: "none",
    cursor: "pointer",
  }));
  return (
    <>
      <Head>
        <title>Korisni tekstovi | Omladinska zadruga Gradac</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="xl">
          <Stack spacing={3}>
            <Stack direction="row" justifyContent="space-between" spacing={4}>
              <Stack spacing={1}>
                <Typography variant="h4">Korisni tekstovi</Typography>
              </Stack>
              <div>
                <Link href="/kreiraj-objavu">
                  <Button
                    startIcon={
                      <SvgIcon fontSize="small">
                        <PlusIcon />
                      </SvgIcon>
                    }
                    variant="contained"
                  >
                    Dodaj
                  </Button>
                </Link>
              </div>
            </Stack>
            <div>
              <Grid
                container
                spacing={3}
                justifyItems="center"
                justifyContent="center"
              >
                {blogs.map((blog) => {
                  <Grid xs={12} md={6} lg={12} key={blog.id}>
                    <Item style={{ width: "350px", marginBottom: "20px" }}>
                      <Image
                        src={blog.avatarUrl}
                        alt="random"
                        style={{
                          width: "100%",
                          height: "165px",
                          objectFit: "cover",
                          borderRadius: "8px",
                        }}
                      />
                      <Typography variant="h5" mt="1em">
                        {blog.title}
                      </Typography>
                      <Typography variant="body1">
                        Explore a quartet of fully available JS APIs for your
                        code
                      </Typography>
                      <Typography mt="1em">
                        September 29, 2022, 5 min read
                      </Typography>
                    </Item>
                  </Grid>;
                })}
              </Grid>
            </div>
          </Stack>
        </Container>
      </Box>
    </>
  );
};
Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
