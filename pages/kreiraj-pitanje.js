import Head from "next/head";
import { useState } from "react";
import { useRouter } from "next/navigation";
import { useFormik } from "formik";
import useMediaQuery from "@mui/material/useMediaQuery";
import * as yup from "yup";
import { Box, Button, TextField} from "@mui/material";
import { useAuth } from "src/hooks/use-auth";
import { Layout as DashboardLayout } from "src/layouts/dashboard/layout";
// import Editor from '../src/sections/editor/text-editor-form'

const Page = () => {
  const router = useRouter();
  const auth = useAuth();
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const [categories, setCategories] = useState([]);
  const [statuses, setStatuses] = useState([]);
  const [genders, setGenders] = useState([]);

  const checkoutSchema = yup.object().shape({
    question: yup.string().required("Ovo polje je obavezno"),
    answer: yup.string().required("Ovo polje je obavezno"),
  });

  const onSubmit = async (values, actions) => {
    console.log(values);
    // const data = {
    //   category: values.category,
    // };
    // createCategory(data);
    await new Promise((resolve) => setTimeout(resolve, 1000));
    actions.resetForm();
  };

  const {
    values,
    handleBlur,
    isSubmitting,
    handleChange,
    handleSubmit,
    touched,
    errors,
  } = useFormik({
    initialValues: {
      question: "",
      answer: ""
    },
    validationSchema: checkoutSchema,
    onSubmit,
  });

  return (
    <>
      <Head>
        <title>Kreiraj pitanje | Omladinska zadruga Gradac</title>
      </Head>
      <Box m="20px">
        <form onSubmit={handleSubmit}>
          <Box
            display="grid"
            gap="30px"
            gridTemplateColumns="repeat(3, minmax(0, 1fr))"
            sx={{
              "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
            }}
          >
            <TextField
              fullWidth
              variant="filled"
              type="text"
              label="Pitanje"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.question}
              name="question"
              error={!!touched.question && !!errors.question}
              helperText={touched.question && errors.question}
              sx={{ gridColumn: "span 3" }}
            />
            {/* <Editor/> */}
          </Box>
          <Box display="flex" justifyContent="end" mt="20px">
            <Button
              variant="contained"
              disabled={isSubmitting}
              type="submit"
            >
              Sacuvaj
            </Button>
          </Box>
        </form>
      </Box>
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
