import {
  Box,
  Button,
  Container,
  Grid,
  Stack,
  SvgIcon,
  Typography,
} from "@mui/material";
import { subDays, subHours } from "date-fns";
import { useCallback, useEffect, useMemo, useState } from "react";

import { CustomersSearch } from "src/sections/customer/customers-search";
import { Layout as DashboardLayout } from "src/layouts/dashboard/layout";
import Head from "next/head";
import Link from "next/link";
import PlusIcon from "@heroicons/react/24/solid/PlusIcon";
import { PostsTable } from "@/src/sections/customer/posts-table";
import { applyPagination } from "src/utils/apply-pagination";
import { useAuth } from 'src/hooks/use-auth';
import { useRouter } from 'next/navigation';

const now = new Date();

const data = [
  {
    id: "5e887ac47eed253091be10cb",
    location: "Beograd",
    avatar: "/assets/avatars/avatar-carson-darrin.png",
    createdAt: subDays(subHours(now, 7), 1).getTime(),
    title: "Administrativni radnik",
    category: "Administracija",
    price: "250 rsd/h",
    workTime: "07:30 - 15:30",
    status: "Studenti",
    gender: "Momci i devojke",
  },
  {
    id: "5e887b209c28ac3dd97f6db5",
    location: "Beograd (Merkator)",
    avatar: "/assets/avatars/avatar-fran-perez.png",
    createdAt: subDays(subHours(now, 1), 2).getTime(),
    title: "Sourcing za jednu community platformu",
    category: "Sourcing",
    price: "400 rsd/h",
    workTime: "Radni dani 08-16h",
    status: "Nezaposleni do 30 god",
    gender: "Momci i devojke",
  },
  {
    id: "5e887b7602bdbc4dbb234b27",
    location: "Beograd",
    avatar: "/assets/avatars/avatar-jie-yan-song.png",
    createdAt: subDays(subHours(now, 4), 2).getTime(),
    title: "Rad u restoranu",
    category: "Ugostiteljstvo",
    price: "250 rsd/h",
    workTime: "Radni dani 08-16h",
    status: "Studenti",
    gender: "Momci",
  },
  {
    id: "5e86809283e28b96d2d38537",
    location: "Beograd",
    avatar: "/assets/avatars/avatar-anika-visser.png",
    createdAt: subDays(subHours(now, 11), 2).getTime(),
    title: "Rad u butiku",
    category: "Laki fizicki poslovi",
    price: "45000 + bonusi",
    workTime: "6.5 sati 6 dana u nedelji",
    status: "Studenti",
    gender: "Devojke",
  },
  {
    id: "5e86805e2bafd54f66cc95c3",
    location: "Nova Pazova",
    avatar: "/assets/avatars/avatar-miron-vitold.png",
    createdAt: subDays(subHours(now, 7), 3).getTime(),
    title: "Lepljenje deklaracija",
    category: "Deklaracije",
    price: "300 rsd/h",
    workTime: "Sutra od 06-14h ili 08-16h",
    status: "Svi",
    gender: "Momci i devojke",
  },
  {
    id: "5e887a1fbefd7938eea9c981",
    location: "Beograd",
    avatar: "/assets/avatars/avatar-penjani-inyene.png",
    createdAt: subDays(subHours(now, 5), 4).getTime(),
    title: "Rad u marketinskoj agenciji",
    category: "Marketing",
    price: "50 000 plata",
    workTime: "Radni dani 09-17h",
    status: "Studenti",
    gender: "Momci i devojke",
  },
  {
    id: "5e887d0b3d090c1b8f162003",
    location: "Krnjesevci",
    avatar: "/assets/avatars/avatar-omar-darboe.png",
    createdAt: subDays(subHours(now, 15), 4).getTime(),
    title: "Deklarisanje",
    category: "Deklaracije",
    price: "350 rsd/h",
    workTime: "Radni dani 08-16h",
    status: "Studenti",
    gender: "Devojke",
  },
  {
    id: "5e88792be2d4cfb4bf0971d9",
    location: "Beograd (Altina)",
    avatar: "/assets/avatars/avatar-siegbert-gottfried.png",
    createdAt: subDays(subHours(now, 2), 5).getTime(),
    title: "Pakovanje paketica",
    category: "Laki fizicki poslovi",
    price: "350 rsd/h",
    workTime: "Sutra od 07-15h ili od 14-22h",
    status: "Svi",
    gender: "Momci i devojke",
  },
  {
    id: "5e8877da9a65442b11551975",
    location: "Beograd",
    avatar: "/assets/avatars/avatar-iulia-albu.png",
    createdAt: subDays(subHours(now, 8), 6).getTime(),
    title: "Administracija",
    category: "Administracija",
    price: "250 rsd/h",
    workTime: "Radni dani 08-16h",
    status: "Studenti",
    gender: "Momci i devojke",
  },
  {
    id: "5e8680e60cba5019c5ca6fda",
    location: "Beograd",
    avatar: "/assets/avatars/avatar-nasimiyu-danai.png",
    createdAt: subDays(subHours(now, 1), 9).getTime(),
    title: "nasimiyu.danai@devias.io",
    category: "Host",
    price: "2500 dnevnica",
    workTime: "Sutra 08-16h",
    status: "Studenti",
    gender: "Devojke",
  },
];

const usePosts = (postList, page, rowsPerPage) => {
  return useMemo(() => {
    return applyPagination(postList, page, rowsPerPage);
  }, [page, rowsPerPage]);
};

const usePostsIds = (posts) => {
  return useMemo(() => {
    return posts.map((post) => post.id);
  }, [posts]);
};

const Page = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [postList, setPostList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const posts = usePosts(postList, page, rowsPerPage);
  const postsIds = usePostsIds(posts);
  const auth = useAuth();
  const token = auth.token;

  useEffect(() => {
    const loadPosts = async () => {
      const postsFromServer = await fetchPosts();
      setPostList(postsFromServer);
    }
    loadPosts();
  }, [])

  const fetchPosts = async () => {
    setIsLoading(true);
    const res = await fetch('http://localhost:8080/api/posts/',
      {
        method: 'GET',
        headers: {
          'Content-type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      });

    if (!res.ok) {
      console.log("Error");
      setIsLoading(false);
    } else {
      const data = await res.json();
      setIsLoading(false);
      return data;
    }
  }

  const handlePageChange = useCallback((event, value) => {
    setPage(value);
  }, []);

  const handleRowsPerPageChange = useCallback((event) => {
    setRowsPerPage(event.target.value);
  }, []);

  return (
    <>
      <Head>
        <title>Customers | Devias Kit</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="xl">
          <Stack spacing={3}>
            <Stack direction="row" justifyContent="space-between" spacing={4}>
              <Stack spacing={1}>
                <Typography variant="h4">Objave</Typography>
              </Stack>
              <div>
                <Link href="/kreiraj-objavu">
                  <Button
                    startIcon={
                      <SvgIcon fontSize="small">
                        <PlusIcon />
                      </SvgIcon>
                    }
                    variant="contained"
                  >
                    Dodaj
                  </Button>
                </Link>
              </div>
            </Stack>
            <Grid xs={12} md={12} lg={8}>
              {isLoading && posts.length > 0 && (
                <PostsTable
                  count={postList.length}
                  items={posts}
                  onPageChange={handlePageChange}
                  onRowsPerPageChange={handleRowsPerPageChange}
                  page={page}
                  rowsPerPage={rowsPerPage}
                  sx={{ height: "100%" }}
                />
              )}
            </Grid>
            {/* <CustomersSearch /> */}
          </Stack>
        </Container>
      </Box>
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
