import {
  Box,
  Button,
  Container,
  Stack,
  SvgIcon,
  Typography,
} from "@mui/material";
import { useCallback, useEffect, useMemo, useState } from "react";

import { CategoriesTable } from "src/sections/category/categories-table";
import { Layout as DashboardLayout } from "src/layouts/dashboard/layout";
import Head from "next/head";
import Link from "next/link";
import PlusIcon from "@heroicons/react/24/solid/PlusIcon";
import { applyPagination } from "src/utils/apply-pagination";
import { useAuth } from 'src/hooks/use-auth';

const useCategories = (categoryList, page, rowsPerPage) => {
  return useMemo(() => {
    return applyPagination(categoryList, page, rowsPerPage);
  }, [page, rowsPerPage]);
};


const Page = () => {
  const [categoryList, setCategoryList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const auth = useAuth();
  const token = auth.token;
  const categories = useCategories(categoryList, page, rowsPerPage);


  useEffect(() => {
    const loadCategories = async () => {
      const categoriesFromServer = await fetchCategories();
      setCategoryList(categoriesFromServer);
    }
    loadCategories();
  }, [])

  const fetchCategories = async () => {
    setIsLoading(true);
    const res = await fetch('http://localhost:8080/api/category/',
      {
        method: 'GET',
        headers: {
          'Content-type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      });

    if (!res.ok) {
      console.log("Error");
      setIsLoading(false);
    } else {
      const data = await res.json();
      setIsLoading(false);
      return data;
    }
  }

  const handlePageChange = useCallback((event, value) => {
    setPage(value);
  }, []);

  const handleRowsPerPageChange = useCallback((event) => {
    setRowsPerPage(event.target.value);
  }, []);

  return (
    <>
      <Head>
        <title>Kategorije | Omladinska zadruga Gradac</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="xl">
          <Stack spacing={3}>
            <Stack direction="row" justifyContent="space-between" spacing={4}>
              <Stack spacing={1}>
                <Typography variant="h4">Kategorije</Typography>
              </Stack>
              <div>
                <Link href="/kreiraj-kategoriju">
                  <Button
                    startIcon={
                      <SvgIcon fontSize="small">
                        <PlusIcon />
                      </SvgIcon>
                    }
                    variant="contained"
                  >
                    Dodaj
                  </Button>
                </Link>
              </div>
            </Stack>
            {!isLoading && categories.length > 0 && (
              <CategoriesTable
                count={categories.length}
                items={categories}
                onPageChange={handlePageChange}
                onRowsPerPageChange={handleRowsPerPageChange}
                page={page}
                rowsPerPage={rowsPerPage}
              />
            )}
          </Stack>
        </Container>
      </Box>
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
